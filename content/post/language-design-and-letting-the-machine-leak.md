+++
title = "Language Design and Letting the Machine Leak"
date = "2017-01-01"
tags = ["Java", "Rust", "C", "Programming language", "PL", "Abstraction", "Leaky Abstractions"]
categories = ["Programming"]
menu = ""
banner = ""
description = ""
images = []
draft = true
+++

C(++) do nothing to abstract away the complexity of the underlying platform
Java et al. try to abstract away the machine with things like garbage collectors, but this makes it easy to forget how the machine really works and what the impact of our code is
Rust has more abstractions than C(++) but it doesn't use them to hide the machine; the machine shines through everywhere, but Rust simply uses rules to keep us from going into dark places
Reminding us that the machine is there might make us write code that better fits how it actually works (cachability)