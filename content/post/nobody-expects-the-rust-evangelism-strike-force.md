+++
title = "Nobody Expects the Rust Evangelism Strike Force"
date = "2017-06-25"
tags = ["Rust","Humor","Meme"]
categories = ["Farts"]
menu = ""
banner = ""
description = ""
images = []
+++

At some point it became a meme to ask people to rewrite their code in Rust.
To the Rust community though, this is a grave insult, as this is a task that is reserved for only our most committed members.
Hence the Rust Evangelism Strike Force was formed, to help preach [the good word](https://www.reddit.com/r/rust/comments/6ewjt5/question_about_rusts_odd_code_of_conduct/didok4h/ "Instructions for being accepted into the Strike Force") and combat this wicked meme.
To try and do my part (I'm not yet accepted into the Force, but I work hard on it!), I have created these scenarios to help the public better understand how the Force operates and what their mission is:

<div style="text-align:center;">
<blockquote class="imgur-embed-pub" lang="en" data-id="a/SlfFK"><a href="//imgur.com/SlfFK"></a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>
</div>