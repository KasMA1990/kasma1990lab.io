+++
title = "Rust is About Better Citizenship"
date = "2017-01-01"
tags = ["Rust", "Marketing"]
categories = ["Programming"]
menu = ""
banner = ""
description = ""
images = []
+++

Steve Klabnik [recently invited](http://words.steveklabnik.com/rust-is-more-than-safety) the Rust community to talk about how we can best sell Rust to others;
not by focusing on Rust's safety wins (which can be an abstract benefit), but on the benefits of using Rust in a wider sense.
So I've decided to chime in with what has sold (and keeps me sold!) on Rust.
I want to start with a small tour through time to help you understand where I'm coming from though, before I start talking about Rust itself.

## How I Arrived at Rust
My first real introduction to programming came in a course I had in the danish equivalent of high school, where I got off the ground with Java.
Then I moved up to university where C# became my primary tool, with some Java from time to time still, and when I finally got my master's degree two years ago, I went on to work with JavaScript and Scala.

In other words, for most of my programming life (relatively short though it is), I've been wrapped in a thick layer of wool.
And although I've been taught the basics (pointers, memory allocation, etc.), most of the time I've been able to safely ignore these as unnecessary details.
The big exception to this was a course I had on C++ towards the end of my university life.
Though we were taught about all the niceties of C++11, there was still a great feeling of having the rug pulled out from under me at that course.
And I mean that in the best possible way, because it gave me a new experience of what programming could be like:
I could feel the bits at my fingertips when I typed.

Not literally of course, that would probably be terrible!
But programming in C++ still made me feel closer to the machine than I had ever really been before, and I felt like I could start writing better software;
software that better respected the resources it was given, and wasn't bogged down by various runtimes which demanded the user's attention.
I could be an all round better software citizen.

But that course also taught me that C++ code is really fucking hard to get right, so I voluntarily went back to my cuddly cage.
It's not like there weren't good things there after all!
For example, it wasn't until I started using Scala and JavaScript that I really discovered the goodness of using collection APIs with lambdas, and Scala quickly became my favorite language.

And then, finally, I discovered Rust.

## Breaking Free
Rust gives me the same feeling that I got from C++:
the feeling that I have a path towards better software citizenship.
But importantly, it does so without making me long for my soft little cave, like C++ does when it routinely sucker punches me as I try to use it.
This also makes me hope that others will get the same feeling as me, and will want to write better behaved software with Rust.

And what is it about Rust that makes me feel this way?
### Design
It's all around well designed;
it's small, coherent, and focused.
The basic building blocks in Rust constantly push you towards writing correct code, without sacrificing the ergonomics of the language.
Additionally, Rust has also translated many of the great concepts I personally came to love with Scala to something that can work in native code, making it very ergonomic and productive.
In short, to me Rust is simply easy on the eye and easy on the mind.
### Access
Rust doesn't need a runtime;
it makes the hardware accessible and doesn't try to hide what the code is actually running on.
We have become used to thinking about our code through several layers of abstraction before we see what the machine is actually doing, but I believe this makes us complacent in some ways.
While our hardware is getting better and better, these abstractions also suck up more and more resources, which means we are making hardware obsolete at a faster pace.
Rust makes many of these abstractions unnecessary;
it gives our users their hardware back, in a manner of speaking.
### Safety
And finally, Rust puts safety first.
You can make safe programs in many languages, but Rust takes it a step further, by forcing you to design for safety.
In an age where the cost of software issues will only escalate, safety should be the default, and Rust does exactly that.

## Conclusion
Rust simply gives me hope that we as an industry now have a path forward for doing better than we do today;
a path towards making software that performs great by default, towards making all the latest security news much more boring, and towards a saner environment for developers and users alike.