+++
title = "Looking in on Rust in 2018"
date = "2018-01-07"
tags = ["Rust", "2018", "Roadmap"]
categories = ["Programming"]
menu = ""
banner = ""
description = ""
images = []
+++

With the [call for #Rust2018 blog posts](https://blog.rust-lang.org/2018/01/03/new-years-rust-a-call-for-community-blogposts.html "Rust blog post describing how the 2018 planning will work.") on how this year's roadmap should look, I figured I'd chime in with a few items.
Now, I'm not using Rust in any shape or form in my day-to-day life (though I'd like to!), but I follow along with the community because so many exciting things happen, and I find Rust itself to be a breath of fresh air.
In other words, my view is shaped mostly by my role as observer (rather than active participant), and the primary need I want to address is to make it easier to convince coworkers and higher-ups to give Rust a shot.

# 2017
I think [the roadmap for 2017](https://blog.rust-lang.org/2017/02/06/roadmap.html "Announcement of the full 2017 roadmap for Rust") was quite excellent.
Not only did it provide concrete goals, but also provided great motivation and guidance for how and where to make contributions, culminating in the impl period.
But looking at 2018, I have an odd feeling that we are not quite done with many of the old goals yet.
Notably, there is still quite a bit of work to be done on async computation, and features like non-lexical lifetimes and `impl Trait` also aren't quite done yet.
At the same time, incremental compilation still has a ways to go before it is truly complete;
and though rustfmt is hitting stable soon, there's still a [paperclip-shaped gap](https://github.com/rust-lang-nursery/rust-clippy "The repository for clippy, the Rust linting program.") in the tooling on the stable channel.

# 2018
There are always new avenues to explore for a language like Rust, but to me, the greatest opportunity we have in 2018 is to take all the good work that was done on the tooling in 2017 and polish it to a mirror shine.
I don't mean to say everyone should be working on this, but I think it would benefit us greatly if we made it a high priority to make these already good implementations the best they can really be.

This would also be an opportunity to look at what else we need to polish up in the project;
for example, while I would love to see progress on getting Rust into WebAssembly, it seems that some of the work needed to do this would be better suited as a concerted effort to make Rust more portable, which could also be tackled as a goal to polish the use of Rust in embedded devices.
With all of this in mind, these are the items I would like to see in the 2018 roadmap:

## "Finish Him!"
The goals from 2017 focusing on the compile cycle and editor experience should be extended into 2018 to reach even higher levels of quality.
That is, while incremental compilation improves our compile times, the transition to incremental is not complete, and the compile times could be pushed down even further.
Adding clippy to the tools available on stable with the ambition of getting its lints integrated into the editor experience would also be excellent.
It's amazingly annoying when you think you're done with a piece of code--everything seems to work--but the build fails because you forgot to run the linter locally;
getting that feedback immediately as you write the code would be much better.

## Portable Rust
2017 focused on removing many of the papercuts that Rust has for general development workflow.
By focusing the same effort on embedded developers in 2018, I believe Rust will stand much stronger outside the native Windows/Linux/macOS trifecta, whether that's on the web or embedded devices.
This is also an important step in making embedded a true first class citizen.

There is already [an RFC](https://github.com/rust-lang/rust/issues/41619 "Tracking issue for RFC1868: A Portability Lint") for making Rust even more portable than it already is, which is a great place to start.
This work should ideally include dealing with [how to propagate `no_std`](https://github.com/rust-lang/rust/issues/38509 "GitHub issue describing the problem with no_std status not propagating down.") down the dependency chain.
Additionally, if we prioritize getting a good chunk of the features that embedded developers need onto the stable channel, Rust should have many fewer sharp edges in this area.
Stretch goals could be something like merging [xargo](https://github.com/japaric/xargo "Repository for xargo") into cargo for maximum ease of use.

## Enterprise Needs
The work on allowing cargo to use alternative crates.io registries is absolutely vital to getting more uptake in the kind of organization I work in.
It's the same story for making it easy to cache crates from a registry;
while there exists [some tools](https://github.com/ChrisMacNaughton/cargo-cacher "Repository for the cargo-cacher tool") for this, there are already established tools like [Nexus Repository](https://www.sonatype.com/nexus-repository-oss "Product page for Sonatype Nexus Repository OSS") for this job.
Tools like Nexus do not yet support cargo and crates.io however, but getting Rust support in there is none the less something I think would open many more doors for Rust usage.
Though I'm not sure how exactly to go about this or if it's even realistic, so it might be a bridge too far for now.

Additionally, I had a [discussion](https://www.reddit.com/r/rust/comments/7hweth/log_040rc1_released/dqwgmgi/?st=jc3aetm2&sh=74a9e799 "Reddit conversation with /u/ksion about structured logging and metrics.") at the announcement of version 0.4 of log-rs about the need for an official metrics-rs crate to complement log-rs.
Such a crate might allow us to have a community wide way of reporting metrics in our applications, which could benefit all our production users if we can find a good design for it.

## Bits and Pieces
Aside from these areas, some things will (and should!) continue forward, simply because of their existing momentum.
That is features like `const fn` and the work on async, but I think macros 2.0 could be a huge boon also;
Rocket has already demonstrated how powerful these macros can be, so it seems that we, more than anything, just need to put focus on them.

# Conclusion
For me, the big story for Rust in 2017 was that it became much more accessible, both in terms of language, but also in tooling and community outreach.
I've chosen to focus mostly on tooling in this post, but many other things will also happen in 2018 no doubt, and I completely trust that whatever direction Rust heads, it will be another great year, based on the sheer strength of the community and the core teams!

