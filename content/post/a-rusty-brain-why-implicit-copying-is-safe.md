+++
title = "A Rusty Brain: Why Implicit Copying Is Safe"
date = "2017-06-08"
tags = ["Rust", "Type System", "Java"]
categories = ["Programming"]
menu = ""
banner = ""
description = ""
images = []
draft = true
+++

I want to talk about a concept in Rust that I've struggled to accept: implicit copying of data in a program, as done with the [`Copy`](https://doc.rust-lang.org/std/marker/trait.Copy.html "Rust documentation for Copy trait") trait.
That is, any data structure that has an implementation of the `Copy` trait will forego usual ownership rules in Rust and create a new and distinct copy of itself instead of being moved.
Oh, and this all happens implicitly so long as such an implementation exists!
So read on if you want to know the epic tale of my struggle, which I've broken down to three areas I've had to explore:

* `Copy` coming from a managed language
* `Copy` and the Rust language
* `Copy` and Rustic code architecture

Let's take them one by one:

## `Copy` Coming From a Managed Language
Forget all about Rust for a second, and assume that, like me, you've been raised in the far away land of Managed Languages.
Now, back in good ol' Managed Languages, objects are usually something you pass around in your code by reference.
Thus if the object is updated somewhere, then everyone who read from the object will see the updated data.
This means that in e.g. Java, you are able to do something like this:

	Date latestUpdateTime = new Date(0);

    void startUpdating() {
		serverUpdateManager.setUpdateTimeObject(latestUpdateTime);
		serverUpdateManager.pollForUpdates();
	}
	
Let's imagine that the `serverUpdateManager` will update our `Date` object to the current system time whenever it receives an update.
Aside from this particular design being all around terrible, you would be able to pass this `Date` around, and anyone reading from it would get the latest time at which an update was received.

Rust works differently though, amongst other things by allowing your data types to implement the `Copy` trait;
instead of all users of `latestUpdateTime` seeing the same data, they would each get their own copy, which would also never be updated again.
So get ready for implicitly leaving stale data all over the place!

#### Enter `Copy`
Okay, that's neither fair nor true, but as a wayward citizen of Managed Languages, that's the instinctive reaction I've had to overcome with `Copy`;
that it hands me a big footgun.
Let's imagine this Rust code emulates our Java code from before, where somebody has derived a `Copy` implementation without knowing that it would break business logic:

    #[derive(Debug, Clone, Copy)] // Move semantics: OFF
	struct CustomDate(u64);

    let mut latest_update_time = CustomDate(0);
	
	fn start_updating() {
		server_update_manager.set_update_time_object(latest_update_time);
		server_update_manager.poll_for_updates();
	}

What the code above does is to create a new (mutable) instance of `CustomDate` containing `0` as its value, bound to `latest_update_time`.
When `set_update_time_object` is called with `latest_update_time`, a copy of its `CustomDate` instance is created and passed as the argument.
In other words, since the `latest_update_time` instance of `CustomDate` was copied, rather than passed by reference, we're not sharing that one instance as we did in Java.

#### It's all in your head
Any reader who knows Rust will know that the last section is blatantly wrong, since removing the `Copy` implementation won't actually give us semantics similar to Java;
it would instead turn the move semantics back on, and the only difference would be that the `latest_update_time` binding won't be valid after the call to `set_update_time_object` (since ownership is transferred into the function).
So neither version of that code, with or without `Copy`, is any kind of equivalent to the Java example, which is really the rub of all this.
You **can** write code in Rust that has a sort-of-similar behavior to the Java code, but Rust makes it hard, because that code would be error-prone, just like the Java code is here.

The weird thing for me has been that I actually knew Rust well enough to know all this for a long time, but somehow whenever I thought of `Copy`, my stomach would instinctively do a flip.
But my gut was wrong, because I was seeing how `Copy` would break the code I write **outside** of Rust, rather than my Rust code.

## `Copy` and the Rust Language
Unlike the first issue, this one does have some technical merit.
Here the issue is simple:
Rust is built for safety and speed, but won't copying data left and right work against that?
Potentially yes, but `Copy` would never have made it into the language if it posed a real risk, so let's establish that it doesn't break things terribly:

#### Copying does not affect memory safety
It seems like `Copy` should actually hurt memory safety, because if you copy pointers, you could easily see problems like use-after-free or data races.
But since the whole point of Rust is to avoid such errors, Rust simply stops you from implementing `Copy` on types that [contain pointers or references](https://doc.rust-lang.org/std/marker/trait.Copy.html#when-cant-my-type-be-copy "Rust documentation for Copy trait").
In other words, the only types that can actually be `Copy` are those that are entirely self-contained/do not have references to any other objects, and implementing `Copy` on something that doesn't fulfill this gives a compile error.

#### Copying is the same as moving
It would be reasonable to fear that copying data is way more computationally expensive than moving data.
But actually, in the current implementation at least, the underlying operation is the same:
[a bit-wise copy operation](https://doc.rust-lang.org/std/marker/trait.Copy.html#whats-the-difference-between-copy-and-clone "Rust documentation for Copy trait").
That is, whether you're moving or copying your data in your Rust code is not significant for performance, because it compiles down to the same operation.
The only difference is that a move will invalidate the old copy that was left behind.

#### Copying is different from referencing
Copying and moving may have the same performance, but moving is only applicable when you're not using references.
Compared to using references, `Copy` can both improve and hurt performance.

First let's say you want to lend out some small piece of data to a function;
if that data has a `Copy` implementation, then you just hand the function a copy, but if you don't want to copy the data, you give a pointer instead (in the form of a reference).
Now, if the data you want to transfer is of the same size or less than a pointer, just copying the data is actually more efficient, since you don't have to do any pointer dereferencing to use it;
the data is simply right there.
But you can have data that is larger (much larger even) than a pointer, and copying that around, as opposed to just handing a pointer to it, can hurt performance.

#### Crate owners control `Copy`
One final aspect of the Rust language that stops `Copy` from creating too much confusion is something called [the orphan rules](http://smallcultfollowing.com/babysteps/blog/2015/01/14/little-orphan-impls "Blog post "Little Orphan Impls" by Niko Matsakis explaining the Rust orphan rules").
The orphan rules are needed because Rust offers the ability to extend any data structure with new trait implementations.
These rules put a limit on that, and basically says that to implement a trait for a given data structure, you must be the owner of either the trait or the data structure.
In other words, you cannot download an external crate for your own project, take a struct from it, and implement a trait from e.g. the standard library for that struct, because you own neither of them.

Niko from the Rust core team [explains it well here](http://smallcultfollowing.com/babysteps/blog/2015/01/14/little-orphan-impls "Blog post "Little Orphan Impls" by Niko Matsakis explaining the Rust orphan rules"), and why it's needed in general.
But specifically for `Copy`, the orphan rules give us peace of mind that we cannot accidentally add a `Copy` implementation to external code, introducing subtle bugs that slip past the compiler.
A `Copy` implementation can only be added by the crate authors themselves.

## `Copy` and Rustic Code Architecture
As we saw under the first issue, Rust makes it harder to write certain types of code than for example Java does, especially because of its ownership semantics.
This is something I consider a net positive, since my humble programming experience so far has been that letting data flow whereever is a notable source of logic errors.
But while ownership combats this, ownership would also seem to be highly affected by `Copy`, so what consequence does `Copy` have for business logic and architecture?
Let's investigate by looking at what happens when a binding is passed to a function, e.g.:

    let my_binding = CustomDate(0);
	take(my_date);

Here is some function `take` that takes a `CustomDate` as input, taking ownership of the struct.
This code would look slightly different depending on what permutations are applied:
instead of taking **ownership*, `take` could **borrow** its input, and the borrow could be made **mutable** instead of **immutable*.
This would also be reflected in the definition of `take`, like so:

<!--|               | Owned                          | Borrowed                         |
| ------------- | ------------------------------ | -------------------------------- |
| **Immutable** | `fn take(date: CustomDate)`    | `fn take(date: &CustomDate)`     |
| **Mutable**   | `fn take(date: mut CustomDate)`| `fn take(date: &mut CustomDate)` |-->
<table class="table-with-left-header">
<thead>
<tr>
<th></th>
<th>Owned</th>
<th>Borrowed</th>
</tr>
</thead>
<tbody>
<tr>
<td style="width:20%"><strong>Immutable</strong></td>
<td style="width:40%" rowspan="2"><code>fn take(date: CustomDate)</code></td>
<td style="width:40%"><code>fn take(date: &CustomDate)</code></td>
</tr>
<tr>
<td><strong>Mutable</strong></td>
<!--<td><code>fn take(mut date: CustomDate)</code></td>-->
<td><code>fn take(date: &mut CustomDate)</code></td>
</tr>
</tbody>
</table>

For more details on why this table looks the way it does, take a look at [an earlier post of mine]({{< relref "ownership-controls-mutability.md" >}}).

#### Comparing semantics
Say we call `take` with `my_binding` in each of these three cases;
what state is `my_binding` in afterwards?
And how does that state differ depending on whether `CustomDate` has a `Copy` implementation or not?

<!--| *Without `Copy`* | Owned                          | Borrowed                         |
| ------------- | ------------------------------ | -------------------------------- |
| **Immutable** | Binding is invalidated         | Binding remains valid            |
| **Mutable**   | Binding is invalidated         | Binding is mutated               |-->
<table class="table-with-left-header">
<thead>
<tr>
	<th><em>Without <code>Copy</code></em></th>
	<th>Owned</th>
	<th>Borrowed</th>
</tr>
</thead>
<tbody>
<tr>
	<td style="width:20%">Immutable</td>
	<td style="width:40%" rowspan="2">Binding is invalidated</td>
	<td style="width:40%">Binding remains valid</td>
</tr>
<tr>
	<td>Mutable</td>
	<!--<td>Binding is invalidated</td>-->
	<td>Binding is mutated but valid</td>
</tr>
</tbody>
</table>
<!--| *With `Copy`* | Owned                          | Borrowed                         |
| ------------- | ------------------------------ | -------------------------------- |
| **Immutable** | Binding remains valid          | Binding remains valid            |
| **Mutable**   | Binding remains valid          | Binding is mutated               |-->
<table class="table-with-left-header">
<thead>
<tr>
	<th><em>With <code>Copy</code></em></th>
	<th>Owned</th>
	<th>Borrowed</th>
</tr>
</thead>
<tbody>
<tr>
	<td style="width:20%">Immutable</td>
	<td style="width:40%" rowspan="2">Binding remains valid</td>
	<td style="width:40%">Binding remains valid</td>
</tr>
<tr>
	<td>Mutable</td>
	<!--<td>Binding remains valid</td>-->
	<td>Binding is mutated but valid</td>
</tr>
</tbody>
</table>

From this comparison, we can tell that only the case where the function tries to take ownership of the data structure is affected.
In other words, the only difference is that passing ownership of a binding won't invalidate the old binding



## Conclusion
It's important to remember that this feeling is based strictly on a mix of conjecture and gut feeling though!
The big question is really if Rust should force ownership on you in cases where it's not actually necessary?
And though my inner rebel wants to say "yes", when the question is phrased like that, the answer feels like it should be, well, "no".
This feeling is only amplified when I look back at my search for answers as to why `Copy` is in Rust;
usually the answer has been something like "Why wouldn't it be?", especially since it would be annoying to have to use borrow syntax when doing common math (e.g. `let y = x * x` would be illegal without `Copy`).

tl;dr
Ownership of `!Copy` types refers only to ownership of memory, and ownership of `Copy` types refers to both memory, resources, and data.




