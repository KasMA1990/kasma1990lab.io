+++
title = "Optimizing Rust Enums for Glory and Performance"
date = "2017-01-01"
tags = ["Rust", "Enum", "Compiler", "Optimization"]
categories = ["Programming"]
menu = ""
banner = ""
description = ""
images = []
draft = true
+++

https://github.com/rust-lang/rust/pull/45225

How can the tag be packed in with the enum data?
	- Does the processor not have to load things in bytes (and not bits)? Does storing the tag and bool together mean there is a CPU time cost for reading the tag?
