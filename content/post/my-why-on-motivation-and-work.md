+++
title = "My \"Why\"; on Motivation and Work"
date = "2017-10-25"
tags = ["Motivation", "Goals"]
categories = ["Personal"]
menu = ""
banner = ""
description = ""
images = []
draft = true
+++

I went to my doctor the other day to try and find out why I am having trouble getting enough sleep.
Having talked to me about it, her immediate take on my problem wasn't that I'm not sleeping enough, but instead that something seems to weigh me down, causing me to be very tired all the time.
This has forced me to recognize something that I've known deep down for a while now: I'm not happy in my job.

Now, it's not like I'm being treated badly in my job, or anything like that.
On the contrary, I've had good talks with my colleagues and managers about how I need to take care of myself and how I can get back on my feet.
But still I'm unhappy.
I'll try to break down the reasons for it:

## Stressing Out
In early 2017 I reached my limit for how much work I could cope with at any one time;
I didn't break under the stress, but I knew I'd reached my tipping point none the less, and I fortunately managed to back down.
I had hoped that my summer vacation would help me get back up, but the vacation had been booked before I knew I needed complete relaxation.
In other words, I believe I'm still paying the price for not taking my recovery seriously enough.
I've had enough of that though, and I am exploring how I can reduce my work hours to get some of my energy back.
We'll see how that turns out.

## Learning is Fun
I recently hit my 3,5 year "anniversary" in my job, and I have known for a while that I wanted to try other things before too long, since the challenges we faced in our project were becoming repetetive.
Thus I recently switched to a new department to try and get a different view of the world;
to see a new code base, a new domain, and a new way of running things.

What I found though, was that even though there are many differences between my old and new departments, they have not been great enough to spur my enthusiasm back to life.
I won't bother with the details of the differences, because I think the important thing for me is that the underlying philosophy is still the same:
move the state of the art in a given domain forward, with software as our tool of choice.
What's wrong with that you ask?
Well, not necessarily anything;
in fact I think it's quite a noble goal and something the company really believes in and strives for.
But I've now become sure that my passion is not in solving the issues of whatever domain my project happens to be targeted at.

## The Passion of the Software Developer
No, my passion is for software itself.
So when I take issue with the aforementioned philosophy, it's not because it's wrong, but because it doesn't fit me.
For me, the software needs to be a much bigger focus!
I want to move the state of the art of software as a whole forward, rather than only use software as a means to moving some other domain forward.


